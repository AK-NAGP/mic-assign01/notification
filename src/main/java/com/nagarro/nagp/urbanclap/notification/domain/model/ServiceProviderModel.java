package com.nagarro.nagp.urbanclap.notification.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ServiceProviderModel {

    private Integer id;

    private String name;

    private String username;

    private Boolean isAvailable;

    private String email;

    private String phone;
}
