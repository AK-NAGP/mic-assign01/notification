package com.nagarro.nagp.urbanclap.notification.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ServiceDetailsModel {

    private Integer id;

    private String serviceName;

    private String serviceType;

    private Double charges;

    private Boolean isServiceAvailable;

}
