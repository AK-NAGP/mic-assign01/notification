package com.nagarro.nagp.urbanclap.notification.jms.consumer;

import com.google.gson.Gson;
import com.nagarro.nagp.urbanclap.notification.domain.enums.OrderStatusEnum;
import com.nagarro.nagp.urbanclap.notification.domain.model.OrderDetailsModel;
import com.nagarro.nagp.urbanclap.notification.service.SendNotification;
import com.nagarro.nagp.urbanclap.notification.service.factory.NotificationFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import java.util.concurrent.CompletableFuture;

@Component
public class NotificationConsumer {

    @Autowired
    private NotificationFactory notificationFactory;

    @Autowired
    private Gson gson;

    @Autowired
    private JavaMailSender emailSender;

    @Value("${spring.mail.username}")
    private String fromEmail;

    @Value("${spring.mail.enable}")
    private Boolean isNotificationEnable;

    @JmsListener(destination = "uc.notification.sendNotification")
    public void sendNotification(String message) {
        OrderDetailsModel orderDetailsModel = gson.fromJson(message, OrderDetailsModel.class);

        String email = orderDetailsModel.getUserDetails().getEmail();

        if (orderDetailsModel.getOrderStatus().equals(OrderStatusEnum.PAYMENT_IN_PROGRESS)) {
            //DO Not Trigger mail for PAYMENT_IN_PROGRESS
            return;
        }

        SendNotification notification = notificationFactory.getNotificationService(orderDetailsModel.getOrderStatus());

        String templateData = notification.getStatusTemplate(orderDetailsModel);
        String subject = notification.getSubject();

        if (orderDetailsModel.getOrderStatus().equals(OrderStatusEnum.PROVIDER_APPROVAL_AWAIT)) {
            email = orderDetailsModel.getServiceProvider().getEmail();
        }

        String finalEmail = email;
        CompletableFuture.runAsync(() -> sendMail(finalEmail, subject, templateData));

    }

    private void sendMail(String email, String subject, String template) {
        SimpleMailMessage message = new SimpleMailMessage();

        message.setFrom(fromEmail);
        message.setTo(email);
        message.setSubject(subject);
        message.setText(template);

        if (isNotificationEnable) {
            emailSender.send(message);
        }
    }

}
