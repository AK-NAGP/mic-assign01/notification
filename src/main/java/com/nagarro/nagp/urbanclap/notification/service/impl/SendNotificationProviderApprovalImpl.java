package com.nagarro.nagp.urbanclap.notification.service.impl;

import com.nagarro.nagp.urbanclap.notification.domain.enums.OrderStatusEnum;
import com.nagarro.nagp.urbanclap.notification.domain.model.OrderDetailsModel;
import com.nagarro.nagp.urbanclap.notification.domain.model.ServiceDetailsModel;
import com.nagarro.nagp.urbanclap.notification.domain.model.ServiceProviderModel;
import com.nagarro.nagp.urbanclap.notification.domain.model.UserDetailsModel;
import com.nagarro.nagp.urbanclap.notification.service.SendNotification;
import org.springframework.stereotype.Service;

@Service
public class SendNotificationProviderApprovalImpl implements SendNotification {
    @Override
    public OrderStatusEnum getStatus() {
        return OrderStatusEnum.PROVIDER_APPROVAL_AWAIT;
    }

    @Override
    public String getSubject() {
        return "NAGP - UC | New Task";
    }

    @Override
    public String getStatusTemplate(OrderDetailsModel orderDetailsModel) {
        StringBuilder stringBuilder = new StringBuilder();
        UserDetailsModel userDetailsModel = orderDetailsModel.getUserDetails();
        ServiceDetailsModel serviceDetailsModel = orderDetailsModel.getServiceDetails();
        ServiceProviderModel serviceProvider = orderDetailsModel.getServiceProvider();

        stringBuilder.append("Dear ").append(serviceProvider.getName());
        stringBuilder.append("\n");
        stringBuilder.append("There is new Order :-").append(orderDetailsModel.getOrderId())
                .append("\n")
                .append("for ").append(serviceDetailsModel.getServiceName())
                .append("\n")
                .append("\n")
                .append("User Details :")
                .append("\n")
                .append("Name : ").append(userDetailsModel.getName())
                .append("\n")
                .append("Email : ").append(userDetailsModel.getEmail())
                .append("\n")
                .append("Phone : ").append(userDetailsModel.getPhone())
                .append("\n")
                .append("Address Details :")
                .append("\n")
                .append(userDetailsModel.getAddressLine1())
                .append("\n")
                .append(userDetailsModel.getAddressLine2())
                .append("\n")
                .append(userDetailsModel.getCity()).append(",").append(userDetailsModel.getState())
                .append("\n")
                .append(userDetailsModel.getPincode());

        if (orderDetailsModel.getPaymentMethod().equals("COD")) {
            stringBuilder.append("\n")
                    .append("Payment to be collected :").append(orderDetailsModel.getCharges());
        }

        stringBuilder.append("\n").append("\n")
                .append("You can Accept Or Deny it !");
        stringBuilder.append("\n");
        stringBuilder.append("Regards,").append("\n").append("Team NAGP");

        return stringBuilder.toString();
    }
}
