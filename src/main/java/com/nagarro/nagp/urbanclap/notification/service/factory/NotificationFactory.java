package com.nagarro.nagp.urbanclap.notification.service.factory;

import com.nagarro.nagp.urbanclap.notification.domain.enums.OrderStatusEnum;
import com.nagarro.nagp.urbanclap.notification.service.SendNotification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

@Component
public class NotificationFactory {

    List<SendNotification> notifications;

    Map<OrderStatusEnum, SendNotification> sendNotificationMap;

    @Autowired
    public NotificationFactory(List<SendNotification> sendNotifications) {
        notifications = sendNotifications;
        sendNotificationMap = new EnumMap<>(OrderStatusEnum.class);
    }

    @PostConstruct
    public void init() {
        notifications.forEach(notification -> sendNotificationMap.put(notification.getStatus(), notification));
    }

    public SendNotification getNotificationService(OrderStatusEnum orderStatusEnum) {
        if (sendNotificationMap.containsKey(orderStatusEnum)) {
            return sendNotificationMap.get(orderStatusEnum);
        } else {
            throw new NullPointerException("Service " + orderStatusEnum.name() + " not registered!");
        }
    }

    public List<SendNotification> getNotificationsServices() {
        return notifications;
    }

}
