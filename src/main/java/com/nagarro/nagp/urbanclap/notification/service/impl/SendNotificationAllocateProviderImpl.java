package com.nagarro.nagp.urbanclap.notification.service.impl;

import com.nagarro.nagp.urbanclap.notification.domain.enums.OrderStatusEnum;
import com.nagarro.nagp.urbanclap.notification.domain.model.OrderDetailsModel;
import com.nagarro.nagp.urbanclap.notification.domain.model.ServiceDetailsModel;
import com.nagarro.nagp.urbanclap.notification.domain.model.ServiceProviderModel;
import com.nagarro.nagp.urbanclap.notification.domain.model.UserDetailsModel;
import com.nagarro.nagp.urbanclap.notification.service.SendNotification;
import org.springframework.stereotype.Service;

@Service
public class SendNotificationAllocateProviderImpl implements SendNotification {
    @Override
    public OrderStatusEnum getStatus() {
        return OrderStatusEnum.SERVICE_IN_PROGRESS;
    }

    @Override
    public String getSubject() {
        return "NAGP - UC | Service Provider Allocated";
    }

    @Override
    public String getStatusTemplate(OrderDetailsModel orderDetailsModel) {
        StringBuilder stringBuilder = new StringBuilder();
        UserDetailsModel userDetailsModel = orderDetailsModel.getUserDetails();
        ServiceDetailsModel serviceDetailsModel = orderDetailsModel.getServiceDetails();
        ServiceProviderModel serviceProvider = orderDetailsModel.getServiceProvider();

        stringBuilder.append("Dear ").append(userDetailsModel.getName());
        stringBuilder.append("\n");
        stringBuilder.append("Your order Id :").append(orderDetailsModel.getOrderId());
        stringBuilder.append("\n");
        stringBuilder.append("for ").append(serviceDetailsModel.getServiceName());
        stringBuilder.append("\n");
        stringBuilder
                .append("We have allocated a Service Provider for your order")
                .append("\n")
                .append("Details :-")
                .append("\n")
                .append("Name :-").append(serviceProvider.getName())
                .append("\n")
                .append("Email :-").append(serviceProvider.getEmail())
                .append("\n")
                .append("Mobile :-").append(serviceProvider.getPhone())
                .append("\n");
        stringBuilder.append("\n");
        stringBuilder.append("Regards,").append("\n").append("Team NAGP");

        return stringBuilder.toString();
    }
}
