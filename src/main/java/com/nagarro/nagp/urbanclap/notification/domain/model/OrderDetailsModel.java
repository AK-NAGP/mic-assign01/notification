package com.nagarro.nagp.urbanclap.notification.domain.model;

import com.nagarro.nagp.urbanclap.notification.domain.enums.OrderStatusEnum;
import lombok.Data;

import java.util.Date;
import java.util.UUID;

@Data
public class OrderDetailsModel {

    private Integer orderId;

    private Double charges;

    private OrderStatusEnum orderStatus;

    private String paymentMethod;

    private UUID paymentId;

    private Date createdOn;

    private Date completedOn;

    private Date paymentOn;

    private ServiceDetailsModel serviceDetails;

    private ServiceProviderModel serviceProvider;

    private UserDetailsModel userDetails;

}
