package com.nagarro.nagp.urbanclap.notification.service.impl;

import com.nagarro.nagp.urbanclap.notification.domain.enums.OrderStatusEnum;
import com.nagarro.nagp.urbanclap.notification.domain.model.OrderDetailsModel;
import com.nagarro.nagp.urbanclap.notification.domain.model.ServiceDetailsModel;
import com.nagarro.nagp.urbanclap.notification.domain.model.UserDetailsModel;
import com.nagarro.nagp.urbanclap.notification.service.SendNotification;
import org.springframework.stereotype.Service;

@Service
public class SendNotificationNewOrderImpl implements SendNotification {

    @Override
    public OrderStatusEnum getStatus() {
        return OrderStatusEnum.IN_PROGRESS;
    }

    @Override
    public String getSubject() {
        return "NAGP - UC | New Order";
    }

    @Override
    public String getStatusTemplate(OrderDetailsModel orderDetailsModel) {
        StringBuilder stringBuilder = new StringBuilder();
        UserDetailsModel userDetailsModel = orderDetailsModel.getUserDetails();
        ServiceDetailsModel serviceDetailsModel = orderDetailsModel.getServiceDetails();

        stringBuilder.append("Dear ").append(userDetailsModel.getName());
        stringBuilder.append("\n");
        stringBuilder.append("Thanks for ordering with us.");
        stringBuilder.append("\n");
        stringBuilder.append("Your order Id is :").append(orderDetailsModel.getOrderId());
        stringBuilder.append("\n");
        stringBuilder.append("Your order is under process. We will soon allocate a Service Provider for your ").append(serviceDetailsModel.getServiceName());
        stringBuilder.append("\n");
        stringBuilder.append("Total Charges:- ").append(orderDetailsModel.getCharges());
        stringBuilder.append("\n");
        stringBuilder.append("Regards,").append("\n").append("Team NAGP");

        return stringBuilder.toString();
    }
}
