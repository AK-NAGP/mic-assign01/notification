package com.nagarro.nagp.urbanclap.notification.service;

import com.nagarro.nagp.urbanclap.notification.domain.enums.OrderStatusEnum;
import com.nagarro.nagp.urbanclap.notification.domain.model.OrderDetailsModel;

public interface SendNotification {

    OrderStatusEnum getStatus();

    String getSubject();

    String getStatusTemplate(OrderDetailsModel orderDetailsModel);
}
