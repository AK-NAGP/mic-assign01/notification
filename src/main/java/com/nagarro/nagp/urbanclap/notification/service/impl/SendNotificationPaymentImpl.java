package com.nagarro.nagp.urbanclap.notification.service.impl;

import com.nagarro.nagp.urbanclap.notification.domain.enums.OrderStatusEnum;
import com.nagarro.nagp.urbanclap.notification.domain.model.OrderDetailsModel;
import com.nagarro.nagp.urbanclap.notification.domain.model.ServiceDetailsModel;
import com.nagarro.nagp.urbanclap.notification.domain.model.ServiceProviderModel;
import com.nagarro.nagp.urbanclap.notification.domain.model.UserDetailsModel;
import com.nagarro.nagp.urbanclap.notification.service.SendNotification;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class SendNotificationPaymentImpl implements SendNotification {

    @Override
    public OrderStatusEnum getStatus() {
        return OrderStatusEnum.PAYMENT_DONE;
    }

    @Override
    public String getSubject() {
        return "NAGP - UC | Payment Success";
    }

    @Override
    public String getStatusTemplate(OrderDetailsModel orderDetailsModel) {
        StringBuilder stringBuilder = new StringBuilder();
        UserDetailsModel userDetailsModel = orderDetailsModel.getUserDetails();
        ServiceDetailsModel serviceDetailsModel = orderDetailsModel.getServiceDetails();

        stringBuilder.append("Dear ").append(userDetailsModel.getName());
        stringBuilder.append("\n");
        stringBuilder.append("Your Payment for order Id :").append(orderDetailsModel.getOrderId());
        stringBuilder.append("\n");
        stringBuilder.append("for ").append(serviceDetailsModel.getServiceName());
        stringBuilder.append("\n");
        stringBuilder.append("Has been successfully done");
        stringBuilder.append("\n");
        stringBuilder.append("Payment Details :-")
                .append("\n")
                .append("Payment Mode :").append(orderDetailsModel.getPaymentMethod())
                .append("\n")
                .append("Payment Id :").append(orderDetailsModel.getPaymentId())
                .append("\n")
                .append("Payment Date :").append(new Date());
        stringBuilder.append("\n");
        stringBuilder.append("Thank you for ordering with us.");
        stringBuilder.append("\n");
        stringBuilder.append("Regards,").append("\n").append("Team NAGP");

        return stringBuilder.toString();
    }
}
