package com.nagarro.nagp.urbanclap.notification.service.impl;

import com.nagarro.nagp.urbanclap.notification.domain.enums.OrderStatusEnum;
import com.nagarro.nagp.urbanclap.notification.domain.model.OrderDetailsModel;
import com.nagarro.nagp.urbanclap.notification.domain.model.ServiceDetailsModel;
import com.nagarro.nagp.urbanclap.notification.domain.model.ServiceProviderModel;
import com.nagarro.nagp.urbanclap.notification.domain.model.UserDetailsModel;
import com.nagarro.nagp.urbanclap.notification.service.SendNotification;
import org.springframework.stereotype.Service;

@Service
public class SendNotificationCompleteImpl implements SendNotification {
    @Override
    public OrderStatusEnum getStatus() {
        return OrderStatusEnum.COMPLETED;
    }

    @Override
    public String getSubject() {
        return "NAGP - UC | Order Completed";
    }

    @Override
    public String getStatusTemplate(OrderDetailsModel orderDetailsModel) {
        StringBuilder stringBuilder = new StringBuilder();
        UserDetailsModel userDetailsModel = orderDetailsModel.getUserDetails();
        ServiceDetailsModel serviceDetailsModel = orderDetailsModel.getServiceDetails();
        ServiceProviderModel serviceProvider = orderDetailsModel.getServiceProvider();

        stringBuilder.append("Dear ").append(userDetailsModel.getName());
        stringBuilder.append("\n");
        stringBuilder.append("Your order Id :").append(orderDetailsModel.getOrderId());
        stringBuilder.append("\n");
        stringBuilder.append("for ").append(serviceDetailsModel.getServiceName());
        stringBuilder.append("\n");
        stringBuilder.append("Has been successfully served by the").append(serviceProvider.getName());
        stringBuilder.append("\n");
        stringBuilder.append("Thank you for ordering with us.");
        stringBuilder.append("\n");
        stringBuilder.append("See you again!!!");
        stringBuilder.append("\n");
        stringBuilder.append("Regards,").append("\n").append("Team NAGP");

        return stringBuilder.toString();
    }
}
